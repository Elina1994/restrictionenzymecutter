/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
package nl.bioinf.eewever.restriction_cutter.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contains all key value pairs of the restriction enzymes.
 * It contains te method addValues to list, which adds the recognition sequence + cutting position to a list.
 * It also contains a map restrictionEnzymes which has as key the restriction enzymes and as value a the lists
 * of method addValues.
 * @author Elina Wever [e.e.wever@st.hanze.nl]
 * @version 0.0.1
 */
public class StoreValues {

   /**
    * Public constructor for class StoreValues.
    */
    public StoreValues() {
    }

    /**
     * EcoRI values stored in a list.
     */
    private final List<Object> ecoRiValues = new ArrayList<>();

    /**
     * BamHI values stored in a list.
     */
    private final List<Object> bamHiValues = new ArrayList<>();
    /**
     * HindIII values stored in a list.
     */
    private final List<Object> hindIIIValues = new ArrayList<>();
    /**
     * TaqI values stored in a list.
     */
    private final List<Object> taqIValues = new ArrayList<>();
    /**
     * NotI values stored in a list.
     */
    private final List<Object> notIValues = new ArrayList<>();
    /**
     * Sau3A values stored in a list.
     */
    private final List<Object> sau3AValues = new ArrayList<>();
    /**
     * HaeIII values stored in a list.
     */
    private final List<Object> haeIIIValues = new ArrayList<>();
    /**
     * EcoRV values stored in a list.
     */
    private final List<Object> ecoRvValues = new ArrayList<>();
    /**
     * PstI values stored in a list.
     */
    private final List<Object> pstIValues = new ArrayList<>();
    /**
     * XbaI values stored in a list.
     */
    private final List<Object> xbaIValues = new ArrayList<>();
    /**
     * MboI values stored in a list.
     */
    private final List<Object> mboIValues = new ArrayList<>();
    /**
     * DraI values stored in a list.
     */
    private final List<Object> draIValues = new ArrayList<>();
    /**
     * SmaI values stored in a list.
     */
    private final List<Object> smaIValues = new ArrayList<>();
    /**
     * XmaI values stored in a list.
     */
    private final List<Object> xmaIValues = new ArrayList<>();
    /**
     * BglII values stored in a list.
     */
    private final List<Object> bglIIValues = new ArrayList<>();

    /**
     * Creates an unique lists for each restriction enzyme. Each restriction enzyme has 2 values : a recognition
     * sequence(String) and a recognition site (Integer). Also each restriction enzyme has its own list.
     *
     * @return restrion enzymelists
     */
    private List addValuesToList() {

        ecoRiValues.add("GAATTC");
        ecoRiValues.add(1);

        bamHiValues.add("GGATCC");
        bamHiValues.add(1);

        hindIIIValues.add("AAGCTT");
        hindIIIValues.add(1);

        taqIValues.add("TCGA");
        taqIValues.add(1);

        notIValues.add("GCGGCCGC");
        notIValues.add(2);

        sau3AValues.add("GATC");
        sau3AValues.add(0);

        haeIIIValues.add("GGCC");
        haeIIIValues.add(2);

        ecoRvValues.add("GATATC");
        ecoRvValues.add(3);

        pstIValues.add("CTGCAG");
        pstIValues.add(4);

        xbaIValues.add("TCTAGA");
        xbaIValues.add(1);

        mboIValues.add("GATC");
        mboIValues.add(0);

        draIValues.add("TTTAAA");
        draIValues.add(3);

        smaIValues.add("CCCGGG");
        smaIValues.add(3);

        xmaIValues.add("CCCGGG");
        xmaIValues.add(1);

        bglIIValues.add("AGATCT");
        bglIIValues.add(1);

        return null;

    }

    /**
     * Map containing restriction sites of enzymes.
     * Keys containing multiple values. All values are stored in an unique list.
     *
     * @return cuttingSites for each restriction enzyme
     */
    public final Map<String, List<Object>> restrictionEnzymes() {
        Map<String, List<Object>> cuttingSites = new HashMap<>();
        cuttingSites.put("EcoRI", ecoRiValues);
        cuttingSites.put("BamHI", bamHiValues);
        cuttingSites.put("HindIII", hindIIIValues);
        cuttingSites.put("TaqI", taqIValues);
        cuttingSites.put("NotI", notIValues);
        cuttingSites.put("Sau3A", sau3AValues);
        cuttingSites.put("HaeIII", haeIIIValues);
        cuttingSites.put("EcoRV", ecoRvValues);
        cuttingSites.put("PstI", pstIValues);
        cuttingSites.put("XbaI", xbaIValues);
        cuttingSites.put("MboI", mboIValues);
        cuttingSites.put("DraI", draIValues);
        cuttingSites.put("SmaI", smaIValues);
        cuttingSites.put("XmaI", xmaIValues);
        cuttingSites.put("BglII", bglIIValues);

        this.addValuesToList();
        return cuttingSites;

    }
}
