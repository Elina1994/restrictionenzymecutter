/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
package nl.bioinf.eewever.restriction_cutter.model;

import java.util.ArrayList;
import java.util.List;

/**
 * This class calculates total GC% per fragment.
 * It contains the method calculatePercentage which does all the math.
 * @author Elina Wever [e.e.wever@st.hanze.nl]
 * @version 0.0.1
 */
public class GcPercentageCalculator {


    /**
     * Public constructor for GcPercentageCalculator.
     */
    public GcPercentageCalculator() {

    }

    /**
     * Integer containing total length of sequence.
     */
    private int totalLength;

    /**
     * List containing GC-percentages per fragment.
     */
    private final ArrayList<String> gcPercentageList = new ArrayList<>();


    /**
     * Getter to retrieve total length of sequence.
     *
     * @return totalLength
     */
    public int getTotalLength() {
        return totalLength;
    }

    /**
     * Getter to retrieve list with GC-percentage.
     *
     * @return gcPercentageList , returns a list with integers
     */
    public ArrayList<String> getGcPercentageList() {
        return gcPercentageList;
    }

    /**
     * Calculates GC% per sequence fragment and returns an list with integers.
     *
     * @param enzymes .
     * @return gcPercentage , GC% of sequence
     * @param sequence .
     */
    public ArrayList calculatePercentage(final String[] enzymes, final String sequence) {

        CutSequences cut = new CutSequences();
        cut.cutWithEnzymes(enzymes, sequence);
        List<String> fragments = cut.getFragmentList();

        String gcPercentage;
        totalLength = sequence.length();
        for (String fragment : fragments) {
            System.out.println(fragment);
            int fragmentLength = fragment.length();

            /*Count occurences of nucleotide G in sequence*/
            double countG = fragment.length() - fragment.replace("G", "").length();
            /*Count occurences of nucleotide C in sequence*/
            double countC = fragment.length() - fragment.replace("C", "").length();
            /*Total amount of G and C occurences*/
            double gcTotal = countG + countC;

            double calculateGC = gcTotal / fragmentLength * 100;
            /*Round percentage to two decimals*/
            gcPercentage = String.format("%.2f", calculateGC) + "%";
            gcPercentageList.add(gcPercentage);
        }
        return gcPercentageList;
    }

}
