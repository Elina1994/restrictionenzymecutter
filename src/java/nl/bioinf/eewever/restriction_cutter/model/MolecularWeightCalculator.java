/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
package nl.bioinf.eewever.restriction_cutter.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class calculates the molecular weight per fragment.
 * It contains the method calculateMolecularWeight which does all the math.
 * It also contains a map called nucleotidesWeight which contains all weights of the DNA nucleotides.
 * @author Elina Wever [e.e.wever@st.hanze.nl]
 * @version 0.0.1
 */
public class MolecularWeightCalculator {

    /**
     * Public constructor for class MolecularWeightCalculator.
    */
    public MolecularWeightCalculator() {

    }

    /**
     * List containing all fragment lengths.
     */
    private final ArrayList<Integer> fragmentLength = new ArrayList<>();

      /**
     * List containing all molecular weights of created fragments.
     */
    private final ArrayList<String> fragmentWeights = new ArrayList<>();

    /**
     * Getter to retrieve all weights of created fragments.
     *
     * @return fragmentWeights .
     */
    public ArrayList<String> getFragmentWeights() {
        return fragmentWeights;
    }


    /**
     * Getter to retrieve fragment lengths.
     *
     * @return fragmentLength.
     */
    public List getFragmentLength() {
        return fragmentLength;
    }

    /**
     * Map containing all weights of DNA/RNA nucleotides.
     *
     * @return map with nucleotides weight
     */
    public final Map<Character, Double> nucleotidesWeight() {
        Map<Character, Double> nucleotideMap = new HashMap<>();
        nucleotideMap.put('A', 135.1267);
        nucleotideMap.put('C', 111.102);
        nucleotideMap.put('G', 151.1261);
        nucleotideMap.put('T', 126.11334);
        nucleotideMap.put('U', 112.08676);
        nucleotideMap.put('N', 0.0);
        nucleotideMap.put('Y', 0.0);
        nucleotideMap.put('X', 0.0);

        return nucleotideMap;
    }

    /**
     * Calculates molecular weight of DNA/RNA sequences.
     *
     * @param fragmentList .
     * @param sequence .
     * @param enzymes .
     * @return molecularWeight
     * @param nucleotideMap , containing weights of all nucleotides
     */
    public List<String> calculateMolecularWeight(final Map nucleotideMap, final List fragmentList,
            final String sequence , final String[] enzymes) {
        CutSequences cut = new CutSequences();
        cut.cutWithEnzymes(enzymes, sequence);
        List<String> fragments = cut.getFragmentList();

        double molecularWeight = 0.0;
        String molWeight = null;

        for (String fragment : fragments) {

            fragmentWeights.add(molWeight);
            /*Set molecularWeight to zero every time one fragment has passed the for loop*/
            molecularWeight = 0.0;

            /*Get fragment lengths and add them to fragmentLength list*/
            int length = fragment.length();
            fragmentLength.add(length);

            for (int nuc = 0; nuc < fragment.length(); nuc++) {
                molecularWeight += (double) nucleotideMap.get(fragment.charAt(nuc));
                molWeight = String.format("%.2f", molecularWeight);
            }
        }

        /*Add last molecular weight to list with fragment weights*/
        fragmentWeights.add(molWeight);
        /*Remove 0 from molecular weight list*/
        fragmentWeights.remove(0);
        return fragmentWeights;
    }
}
