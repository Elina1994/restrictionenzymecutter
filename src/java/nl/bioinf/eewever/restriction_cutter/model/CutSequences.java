/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
package nl.bioinf.eewever.restriction_cutter.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is the "main" class of the application , it cuts the sequence in different fragments.
 * It contains the method cutWithEnzymes which cuts the sequence in parts depending on
 * which enzyme(s) was/were selected.
 * @author Elina Wever [e.e.wever@st.hanze.nl]
 * @version 0.0.1
 */
public final class CutSequences {

    /**
     * Public constructor for class CutSequences.
     */
    public CutSequences() {
        this.values = new StoreValues();
    }

    /**
     * Retrieve all keys and values of restriction enzymes.
     */
    private final StoreValues values;

    /**
     * List containing all fragments when sequence is cut.
     */
    private final ArrayList<String> fragmentList = new ArrayList<>();

    /**
     * Getter to retrieve fragments.
     *
     * @return fragmentList containing all fragments
     */
    public ArrayList getFragmentList() {
        return fragmentList;

    }

    /**
     * This method cuts sequence with the selected enzymes.
     * It returns a fragment list containing a sequence cut in pieces
     *
     * @param enzymes , list containing all enzymes
     * @param sequence , sequence to digest
     * @return fragmentList
     */
    public ArrayList<String> cutWithEnzymes(final String[] enzymes, String sequence) {

        /*Map containing restriction enzymes*/
        Map restrictionEnzymes = values.restrictionEnzymes();

        /*Create first substring variable*/
        String firstSequence = null;

        for (Object key : enzymes) {

            List valuesList = (List) restrictionEnzymes.get(key);
            /*Retrieve recognition sequence*/
            String recSite = (String) valuesList.get(0);
            /*Retrieve cutting position*/
            int cutSite = (Integer) valuesList.get(1);
            Pattern pattern = Pattern.compile(recSite);
            Matcher match = pattern.matcher(sequence);

            /*Search for matches and determine cutting position*/
            while (match.find()) {

                int cuttingStart = match.start() + cutSite;
                /*Cut from begin till start match*/
                firstSequence = sequence.substring(0, cuttingStart);
                /*Add fragments to list when longer than 0 nucleotides*/
                if (firstSequence.length() > 0) {
                    fragmentList.add(firstSequence);
                }
                /*Cut from match until end of sequence*/
                sequence = sequence.substring(cuttingStart);
            }

        }
        if (sequence.length() > 0) {
            fragmentList.add(sequence);
        }

        return fragmentList;

    }

}
