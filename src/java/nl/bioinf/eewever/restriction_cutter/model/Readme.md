## Application Details ##

This application is a sequence cutting application and returns different types of output.
The input sequence is any DNA sequence which contains recognition patterns for
restriction enzymes.

Supported enzymes are provided on the webapplication and can be selected by
checkboxes. The supported enzymes are : [EcoRI, BamHI, HindIII, TaqI, NotI, 
Sau3A, HaeIII, EcoRV, PstI, XbaI, MboI, DraI, SmaI, XmaI, BglII]

After selection of enzymes and input of a sequence the sequence is processed and
returns information about the created fragments. Information that is returned 
is : molecular weight, GC-percentage , fragment length , start + stop position
of the fragments.

## Usage details ##

**The input**
```
Sequence to paste within the form : AACCGGTTCGTTCGACGTTGTAACCGGTT
A selected enzyme : TaqI
```
**The output**
```
Fragments                    Fragment-Length GC-Percentage Molecular-Weight Start Stop
5'- AACCGGTTCGTT -'3             12 bp          50,00%       1561,39          1    12
5'- CGACGTTGTAACCGGTT-'3         17 bp          52,94%       2235,99          13   29
```                         


