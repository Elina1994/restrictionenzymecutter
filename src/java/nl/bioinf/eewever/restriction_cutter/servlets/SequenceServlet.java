/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
package nl.bioinf.eewever.restriction_cutter.servlets;

import java.io.IOException;
import java.util.Arrays;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.eewever.restriction_cutter.model.CutSequences;
import nl.bioinf.eewever.restriction_cutter.model.GcPercentageCalculator;
import nl.bioinf.eewever.restriction_cutter.model.MolecularWeightCalculator;
import nl.bioinf.eewever.restriction_cutter.model.StartStop;

/**
 *
 * @author Elina Wever [e.e.wever@st.hanze.nl]
 * @version 0.0.1
 */
@WebServlet(name = "SequenceServlet", urlPatterns = {"/input.do"})
public class SequenceServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String sequences = request.getParameter("sequence").toUpperCase().replace(" ", "");
        String[] enzymes = request.getParameterValues("enzyme");

        HttpSession session = request.getSession();
        session.setMaxInactiveInterval(10);

        if (enzymes == null) {
            String errorMessage = "Warning : no enzymes selected, please select at least one !";
            request.setAttribute("errorMessage", errorMessage);

            RequestDispatcher view = request.getRequestDispatcher("/jsp/userInput.jsp");
            view.forward(request, response);

        } else if (sequences.length() < 5) {
            String lengthError = "Warning: Your entered sequence should be longer then 5 nucleotides !";
            request.setAttribute("lengthError", lengthError);

            RequestDispatcher view = request.getRequestDispatcher("/jsp/userInput.jsp");
            view.forward(request, response);
        } else {

            GcPercentageCalculator calculator = new GcPercentageCalculator();
            calculator.calculatePercentage(enzymes, sequences);

            CutSequences cutting = new CutSequences();
            cutting.cutWithEnzymes(enzymes, sequences);
            MolecularWeightCalculator mol = new MolecularWeightCalculator();
            mol.calculateMolecularWeight(mol.nucleotidesWeight(), cutting.getFragmentList(), sequences, enzymes);

            StartStop startstop = new StartStop();
            startstop.calculateStartStop(sequences, cutting.getFragmentList(), enzymes);

            request.setAttribute("sequences", sequences);
            request.setAttribute("enzymes", Arrays.toString(enzymes));

            /*Set information about inserted sequence*/
            request.setAttribute("gcpercentage", calculator.getGcPercentageList());
            request.setAttribute("length", calculator.getTotalLength());
            request.setAttribute("fragments", cutting.getFragmentList());
            request.setAttribute("fragmentWeights", mol.getFragmentWeights());
            request.setAttribute("lengths", mol.getFragmentLength());
            request.setAttribute("start", startstop.getStartList());
            request.setAttribute("stop", startstop.getStopList());

            RequestDispatcher view = request.getRequestDispatcher("/jsp/output.jsp");
            view.forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods.
    //Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>

}
