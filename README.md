### What is this repository for? ###

This repository is for the web application Restriction Enzyme Cutter.

### Used Software ###

* Java 8
* Netbeans IDE 8.0.2
* Jquery 2.1.4
* Jquery UI
* CSS3
* Apache TomCat

### How do I get set up? ###

To get this app up and running there are are two .jar files which should be added to
the libraries. These .jar files are found in the downloads sections of this repo.

When downloaded you have to add these files to the libraries of this project. Go to libraries >> right click >> add JAR/folders>>
select folder where these jars are located >> select and add them.

Also make sure you have downloaded jquery and jquery ui. When installed all required libraries and other needed programs, you go to
userInput.jsp , run this file and the application will be started ! 

## Application Details ##

This application is a sequence cutting application and returns different types of output.
The input sequence is any DNA sequence which contains recognition patterns for
restriction enzymes.

Supported enzymes are provided on the webapplication and can be selected by
checkboxes. The supported enzymes are : [EcoRI, BamHI, HindIII, TaqI, NotI, 
Sau3A, HaeIII, EcoRV, PstI, XbaI, MboI, DraI, SmaI, XmaI, BglII]

After selection of enzymes and input of a sequence the sequence is processed and
returns information about the created fragments. Information that is returned 
is : molecular weight, GC-percentage , fragment length , start + stop position
of the fragments.

## Usage details ##

**The input**
```
Sequence to paste within the form : AACCGGTTCGTTCGACGTTGTAACCGGTT
A selected enzyme : TaqI
```
**The output**
```
Fragments                    Fragment-Length GC-Percentage Molecular-Weight Start Stop
5'- AACCGGTTCGTT -'3             12 bp          50,00%       1561,39          1    12
5'- CGACGTTGTAACCGGTT-'3         17 bp          52,94%       2235,99          13   29
```