/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */

/*It checks if the input contains any illegal characters*/
function checkInput() {

    var getVal = document.getElementById("inputSequence").value;
    
    /*If  illegal characters found stay on the same page and report error*/
    if ((/[^acgtyrx]/i).test(getVal)) {
       
        alert("invalid characters found !");
        return false;
    }

};

