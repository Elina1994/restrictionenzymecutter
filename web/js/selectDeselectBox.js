/*
 * Copyright (c) 2015 Elina Wever [e.e.wever@st.hanze.nl].
 * All rights reserved.
 */
window.onload = selectDeselect();
function selectDeselect(){
$('#select').click(function() {
    /*Check all boxes if not already checked*/
    if ($(this).is(':checked')) {
     $('#enzymeOptions input[type=checkbox]').prop("checked", true); 
            
    /*Deselect all checkboxes*/
    }else {
        $('#enzymeOptions input[type=checkbox]').prop("checked", false); 
              
    }
}); }