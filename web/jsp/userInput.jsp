<%-- 
    Document   : userInput
    Created on : 18-dec-2015, 16:34:47
    Author     : Elina Wever [e.e.wever@st.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sequence Cutting Tool</title>
        <base href="${initParam.baseURL}">
        <link href="css/webStyleSheet.css" rel="stylesheet" type="text/css"/>
        <jsp:include page = "/includes/banner.jsp"/>

        <script src="js/lib/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="js/lib/jquery-ui.min.js" type="text/javascript"></script>
        <script src="js/selectDeselectBox.js" type="text/javascript"></script>
        <script src="js/formValidator.js" type="text/javascript"></script>
    </head>
    <body>
        <b>! There is a major bug inside this program which I haven't been able to fix<br/>
            Despite of my efford this whole weekend........ :( <br/>
            The program crashes when more than 2 cuttingsites of one enzyme is found. <br/>
            A sequence which does work at least with two enzymes(BamHI + DraI) or all Enzymes :<br/>
            CCGGATCCAACCGGATCCACCCCCTTTAAAAAGGTTTAACCGGATCTTTTTTTTT . <br/>
        </b>

        <div id = "container"><h2>Sequence cutter program</h2>

            <p>This program digests any given DNA sequence and returns a fragmented sequence.<br/>
                It also calculates GC-content, molecular weight, fragment length and the start + stop <br/>
                position of the created fragments. No header should be included when submitting the sequence. <br/>
                Also be aware that the character "Enter" is not allowed in this form, remove your enters before<br/>
                you submit or else the program won't process the entered sequence. <br/>


            </p>

            <p>
                Below you will see a form and the supported restriction enzymes of this program.<br/>
                You can easily resize the form by dragging the right corner at the bottom of the form.<br/>
                Enter your DNA sequence within the form, select the desired restriction enzymes and hit the <br/>
                submit sequence button. Your submitted sequence will then be processed.<br/> 

            </p>
            <div id = "error">${requestScope.lengthError}</div> <br/>
            <form action= "input.do" method = "POST" id = "form" >

                <td> Your Sequence : </td><textarea name = "sequence"  id ="inputSequence" cols = "45" rows = "2" 
                                                    type ="text" required  placeholder = "Enter a valid DNA sequence"
                                                    ></textarea></td>
                <td></td><td><input type="submit" value = "Submit Sequence" id = "button" onclick = "return checkInput()"></td><br/>

                <h3>Select restriction enzymes to cut with : </h3>

                <div id ="enzymeOptions"> ${requestScope.errorMessage} <br/>

                    <td>EcoRI</td><td><input   type ="checkbox" name = "enzyme" value = "EcoRI" ></td>
                    <td>BamHI</td><td><input   type ="checkbox" name = "enzyme" value = "BamHI"></td>
                    <td>HindIII</td><td><input type ="checkbox" name = "enzyme" value = "HindIII"></td><br/>
                    <td>TaqI</td><td><input    type ="checkbox" name = "enzyme" value = "TaqI"></td>
                    <td>NotI</td><td><input    type ="checkbox" name = "enzyme" value = "NotI"></td>
                    <td>Sau3A</td><td><input   type ="checkbox" name = "enzyme" value = "Sau3A"></td><br/>
                    <td>HaeIII</td><td><input  type ="checkbox" name = "enzyme" value = "HaeIII"></td>
                    <td>EcoRV</td><td><input   type ="checkbox" name = "enzyme" value = "EcoRV"></td>
                    <td>PstI</td><td><input    type ="checkbox" name = "enzyme" value = "PstI"></td><br/>
                    <td>XbaI</td><td><input    type ="checkbox" name = "enzyme" value = "XbaI"></td>
                    <td>MboI</td><td><input    type ="checkbox" name = "enzyme" value = "MboI"></td>
                    <td>DraI</td><td><input    type ="checkbox" name = "enzyme" value = "DraI"></td><br/>
                    <td>SmaI</td><td><input    type ="checkbox" name = "enzyme" value = "SmaI"></td>
                    <td>XmaI</td><td><input    type ="checkbox" name = "enzyme" value = "XmaI"></td>
                    <td>BglII</td><td><input   type ="checkbox" name = "enzyme" value = "BglII"></td><br/><br/>

                    <i>Double click on the select/deselect box to activate and use it.</i> <br/><br/>
                    <td></td>Select/Deselect all enzymes<td><input type = "checkbox" id ="select"  onclick = "return selectDeselect()"></td>  
                </div> 

            </form></div>            
            <jsp:include page = "/includes/footer.jsp"/>
    </body>

</html>
