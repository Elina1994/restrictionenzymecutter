<%-- 
    Document   : output
    Created on : 23-dec-2015, 10:58:50
    Author     : Elina Wever [e.e.wever@st.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <base href="${initParam.baseURL}">
        <link href="css/webStyleSheet.css" rel="stylesheet" type="text/css"/>

    <div><img class = "displayed" src="images/DNA.jpg" id = "banner"></div>
    <title>Sequence Analyzing Tool</title>
</head>
<body>
    <!--Display standard information, like selected enzymes-->
    <h3>You've succesfully entered a sequence !</h3>
    <p> Entered sequence is : 5' - ${requestScope.sequences} - '3<br/> 
        Selected enzymes are : ${requestScope.enzymes}<br/>
        Total sequence length : ${requestScope.length} bp<br/><br/>
        <jsp:include page = "/includes/footer.jsp"/>
    </p>

    <!--Place each piece of information in separate tables to make sure the layout 
    is like any normal table-->

    <!--Get all created fragments and add to table-->
    <table class = "position">   

        <thead>
            <tr><th>Fragments</th></tr>
        </thead>

        <tbody>
            <c:forEach var = "frag"  items = "${requestScope.fragments}">

                <tr><td> 5'- ${frag} -'3 </td></tr>


            </c:forEach>
        </tbody>        
    </table> 

    <!--Get each fragments length and add to table-->
    <table class = "position">  
        <thead>
            <tr><th>Fragment Length</th></tr>
        </thead> 

        <tbody>
            <c:forEach var = "length" items = "${requestScope.lengths}">

                <tr><td> ${length} bp</td></tr>

            </c:forEach> 
        </tbody></table>       

    <!--Get each fragments GC Percentage and add to table-->
    <table class = "position">

        <thead>
            <tr><th>GC-Percentage</th></tr>
        </thead>

        <tbody>
            <c:forEach var = "gcPercentage" items = "${requestScope.gcpercentage}">  

                <tr><td>${gcPercentage}</td></tr>

            </c:forEach>
        </tbody>
    </table>

    <!--Get each fragments molecular weight and add to table-->
    <table class ="position">

        <thead>
        <th>Molecular Weight</th>
    </thead>

    <tbody>
        <c:forEach var = "molWeight" items = "${requestScope.fragmentWeights}">

            <tr><td>${molWeight}</td></tr>

        </c:forEach>
    </tbody>    
</table>  


<!--Get each fragments start position and add to table-->
<table class ="position"> 
    <thead> 
    <th>Start</th>
</thead>
<tbody>
    <c:forEach var = "start" items = "${requestScope.start}">

        <tr><td>${start}</td></tr>

    </c:forEach>
</tbody></table>

<!--Get each fragments stop positon and add to table-->

<table class ="position">
    <thead> 
    <th>Stop</th> 
</thead>
<tbody>
    <c:forEach var = "stop" items = "${requestScope.stop}">

        <tr><td>${stop}</td></tr>

    </c:forEach>
</tbody></table><br/><br/>


</body>

</html>
